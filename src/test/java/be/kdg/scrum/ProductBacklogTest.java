package be.kdg.scrum;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Author: Jan de Rijke
 */
public class ProductBacklogTest {
	private Backlog bl = new Backlog();
	private UserStory currentUserStory = null;

	@Given("User Stories")
	public void userStories(List<UserStory> list) {
		list.forEach(bl::addStory);
	}

	@DataTableType
	public UserStory userStoryConverter(Map<String, String> row) {
		return new UserStory(
			Integer.parseInt(row.get("storyId")),
			row.get("text"),
			row.get("notes"),
			Integer.parseInt(row.get("priority")));
	}

	@When("I add {int} new user stories")
	public void iAddNewUserStories(int number) {
		for (int i = 0; i < number; i++) {
			bl.addStory(new UserStory(100 + number, "dummy" + number, "note " + number, 1));
		}
	}

	@Then("there are {int} user stories in the backlog")
	public void thereAreUserStoriesInTheBacklog(int number) {
		assertEquals(bl.countStories(), number);
	}


	@When("I add user story {int} with priority {int}")
	public void iAddUserStoryWithPriority(int id, int priority) {
		bl.addStory(new UserStory(id, "geen omschrijving", "geen toevoeging", priority));
	}

	@And("user story {int} has priority {int}")
	public void userStoryHasPriority(int id, int priority) {
		assertEquals(priority, bl.getStory(id).getPriority());
	}

	@When("I ask to get user story with id {int}")
	public void iAskToGetUserStoryWithId(int id) {
		currentUserStory = bl.getStory(id);
	}

	@Then("the programs returns the user story with id {int}")
	public void theProgramsReturnsTheUserStoryWithId(int id) {
		assertEquals(id, currentUserStory.getId());
	}


}

