# Put your feature and scenario's in this file
Feature: Product Backlog

  Background:
    Given User Stories
      |storyId|text|notes|priority|
      |1|As a rabbit I want to eat carrots so that my sight will become better|No comment|1|
      |2|As a rabbit I want to make carrotcake so that my birthday party will be a success|No comment|2|
      |3|As a fox I want to grow carrots so that there are  rabbits galore|No comment|2|


  Scenario: Add new story to existing Backlog
    When I add 1 new user stories
    Then there are 4 user stories in the backlog

  Scenario: Add existing story to existing Backlog
    When I add user story 1 with priority 3
    Then there are 3 user stories in the backlog
    And user story 1 has priority 1

  Scenario: Retrieve an existing user story
    When I ask to get user story with id 1
    Then the programs returns the user story with id 1